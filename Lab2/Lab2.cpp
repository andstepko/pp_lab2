// Lab2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "windows.h"
#include "math.h"
#include "time.h"

using namespace std;

int* generateRandomArray(int n)
{
	int* result = (int*)malloc(n*sizeof(int));

	for(int i = 0; i < n; i++)
	{
		result[i] = (rand() % 15000) - 7500;
	}
	return result;
}

float* generateRandomFloatArray(int n)
{
	float* result = (float*)malloc(n*sizeof(float));

	for(int i = 0; i < n; i++)
	{
		result[i] = ((rand() % 15000) - 7500) / 100.0;
	}
	return result;
}

bool* generateRandomBoolArray(int n)
{
	bool* result = (bool*)malloc(n*sizeof(bool));

	for(int i = 0; i < n; i++)
	{
		result[i] = rand() % 2;
	}
	return result;
}

int* generateOneZeroRandomArray(int n)
{
	int* result = (int*)malloc(n*sizeof(int));

	for(int i = 0; i < n; i++)
	{
		result[i] = (rand() % 3) - 1;
	}
	return result;
}

int* clone(int* data, int n)
{
	int* result = (int*)malloc(n * sizeof(int));

	for(int i = 0; i < n; i++)
	{
		result[i] = data[i];
	}
	return result;
}
float* clone(float* data, int n)
{
	float* result = (float*)malloc(n * sizeof(float));

	for(int i = 0; i < n; i++)
	{
		result[i] = data[i];
	}
	return result;
}

// Positive
int positiveNumbers(int* data, int n)
{
	int result = 0;

	for(int i = 0; i < n; i++)
	{
		if(data[i] > 0)
		{
			result += 1;
		}
	}
	return result;
}

int smartPositiveNumbers(int* data, int n)
{
	int result = 0;

	for(int i = 0; i < n; i++)
	{
		result += (data[i] > 0);
	}
	return result;
}

// Buble
void stupidBubleSort(int* data, int n)
{
	int steps = 0;
	int ifs = 0;
	int stops = 0;

	for(int i = 0; i < n - 1; i++)
	{
		for(int j = 0; j < n - 1; j++)
		{
			steps++;
			if(data[j] < data[j + 1])
			{
				ifs++;
				int t = data[j];
				data[j] = data[j + 1];
				data[j + 1] = t;
				continue;
			}
			stops++;
		}
	}

	printf("stupid steps==>%i\n", steps);
	printf("stupid ifs==>%i\n", ifs);
	printf("stupid stops==>%i\n", stops);
}

void smartBubleSort(int* data, int n)
{
	int steps = 0;
	int ifs = 0;
	int stops = 0;

	for(int i = 0; i < n - 1; i++)
	{
		for(int j = i; j >= 0; j--)
		{
			steps++;
			if(data[j] < data[j + 1])
			{
				ifs++;
				int t = data[j];
				data[j] = data[j + 1];
				data[j + 1] = t;
				continue;
			}
			stops++;
		}
	}

	printf("smart steps==>%i\n", steps);
	printf("smart ifs==>%i\n", ifs);
	printf("smart stops==>%i\n", stops);
}

void myBubleSort(int* data, int n)
{
	int steps = 0;
	int ifs = 0;
	int stops = 0;

	for(int i = 0; i < n - 1; i++)
	{
		for(int j = n - 1; j > i; j--)
		{
			steps++;
			if(data[j - 1] < data[j])
			{
				ifs++;
				int t = data[j];
				data[j] = data[j + 1];
				data[j + 1] = t;
				continue;
			}
			stops++;
		}
	}

	printf("my steps==>%i\n", steps);
	printf("my ifs==>%i\n", ifs);
	printf("my stops==>%i\n", stops);
}

// Polynom
int* stupidPlusMinusMultPolynom(int* first, int n1, int* second, int n2)
{
	int n3 = n1 + n2 - 1;
	int* result = (int*)calloc(n3, sizeof(int));

	for(int i = 0; i < n2; i++)
	{
		for(int j = 0; j < n1; j++)
		{
			if(second[i] > 0)
			{
				result[j + i] += first[j];
			}
			else if(second[i] < 0)
			{
				result[j + i] -= first[j];
			}
		}
	}
	return result;
}

int* generalMultPolynom(int* first, int n1, int* second, int n2)
{
	int n3 = n1 + n2 - 1;
	int* result = (int*)calloc(n3, sizeof(int));

	for(int i = 0; i < n2; i++)
	{
		for(int j = 0; j < n1; j++)
		{
			result[j + i] += first[j] * second[i];
		}
	}
	return result;
}

int* smartPlusMinusMultPolynom(int* first, int n1, int* second, int n2)
{
	// Doesn't touch zero positions.
	int n3 = n1 + n2 - 1;
	int* result = (int*)calloc(n3, sizeof(int));

	for(int i = 0; i < n2; i++)
	{
		if(second[i])
		{
			for(int j = 0; j < n1; j++)
			{
				if(second[i] >> ((sizeof(int) * 8) - 1))
				{
					result[j + i] -= first[j];
				}
				else
				{
					result[j + i] += first[j];
				}
			}
		}
	}
	return result;
}

int* superSmartPlusMinusMultPolynom(int* first, int n1, int* second, int n2)
{
	// Removes 1 if-else.
	int n3 = n1 + n2 - 1;
	int* result = (int*)calloc(n3, sizeof(int));

	for(int i = 0; i < n2; i++)
	{
		if(second[i])
		{
			for(int j = 0; j < n1; j++)
			{
				result[j + i] += first[j] * second[i];
			}
		}
	}
	return result;
}

int* buggySuperSmartPlusMinusMultPolynom(int* first, int n1, int* second, int n2)
{
	// Removes 1 if-else.
	int n3 = n1 + n2 - 1;
	int* result = (int*)calloc(n3, sizeof(int));

	for(int i = 0; i < n2; i++)
	{
		if(!second[i])
		{
		}
		else
		{
			for(int j = 0; j < n1; j++)
			{
				result[j + i] += first[j] * second[i];
			}
		}
	}
	return result;
}

// Round
void stupidRound(float* data, int n)
{
	float smallerPart;
	for(int i = 0; i < n; i++)
	{
		smallerPart = (data[i] - (int)data[i]);
		if(data[i] >= 0)
		{
			if(smallerPart >= 0.5)
			{
				data[i] = (int)data[i] + 1;
			}
			else
			{
				data[i] = ((int)data[i]);
			}
		}
		else
		{
			// data[i] < 0
			if(smallerPart <= -0.5)
			{
				data[i] = (int)data[i] - 1;
			}
			else
			{
				data[i] = (int)data[i];
			}
		}
	}
}

void smartRound(float* data, int n)
{
	// smallerPart implemented
	float smallerPart;
	for(int i = 0; i < n; i++)
	{
		smallerPart = (data[i] - (int)data[i]);
		data[i] = (int)data[i]
			+ (smallerPart >= 0.5) - (smallerPart <= -0.5);
	}
}

void superSmartRound(float* data, int n)
{
	// smallerPart implemented
	float smallerPart;
	for(int i = 0; i < n; i++)
	{
		data[i] = (int)(data[i] + 0.5 - (((int)data[i]) >> (sizeof(int) * 8 - 1)));
	}
}

// Predict
int predict(bool* data, int n)
{
	bool* table = new bool[32]();
	int errors = 0;
	byte cur;

	for(int i = 0; i < 4; i++)
	{
		errors += data[i];
	}

	for(int i = 4; i < n; i++)
	{
		cur = 0;
		for(int j = i - 4; j < i; j++)
		{
			cur |= (data[j] << (i - j - 1));
		}
		errors += table[cur * 2] != data[i];

		if(data[i])
		{
			// increment
			if(table[cur * 2 + 1])
			{
				if(!table[cur * 2])
				{
					// 01
					table[cur * 2] = true;
					table[cur * 2 + 1] = false;
				}
				// 11  ---> no op.
			}
			else
			{
				// lower bit is 0
				table[cur * 2 + 1] = true;
			}
		}
		else
		{
			// decrement
			if(table[cur * 2 + 1])
			{
				// lower bit is 1
				table[cur * 2 + 1] = false;
			}
			else
			{
				// lower bit is 0
				if(table[cur * 2])
				{
					// 10
					table[cur * 2] = false;
					table[cur * 2 + 1] = true;
				}
				// 00  ---> no op.
			}
		}
	}
	return errors;
}


int _tmain(int argc, _TCHAR* argv[])
{
	int N = 100000000;

	srand(time(0));
	//int* ar = generateRandomArray(N);	

	__int64 frequency;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);
	__int64 start, finish;


	printf("Methods:\n");
	// Positives
	/*int positiveCount;

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	positiveCount = positiveNumbers(ar, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Stupid positive time==>%f ms positives==>%i\n", (finish - start) * 1000.0 / frequency,
		positiveCount);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	positiveCount = smartPositiveNumbers(ar, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Smart positive time==>%f ms positives==>%i\n", (finish - start) * 1000.0 / frequency,
		positiveCount);*/


	// Buble
	/*int* ar1 = clone(ar, N);
	int* ar2 = clone(ar, N);
	int* ar3 = clone(ar, N);
	
	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	stupidBubleSort(ar1, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Stupid time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	smartBubleSort(ar2, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Smart time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	myBubleSort(ar3, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("My time==>%f ms\n", (finish - start) * 1000.0 / frequency);*/


	// Polynom
	/*int first[] = {4, -3, -2, 1, 7};
	int second[] = {-1, 1, 0};

	int* result = generalMultPolynom(first, 5, second, 3);
	for(int i = 0; i < 7; i++)
	{
		printf("%i\n", result[i]);
	}*/
	
	/*int N2 = N / 10;
	int* oneZeroArray = generateOneZeroRandomArray(N2);
	
	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	generalMultPolynom(ar, N, oneZeroArray, N2);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("General mult polynom time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	stupidPlusMinusMultPolynom(ar, N, oneZeroArray, N2);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("StupidPlusMinus mult polynom time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	smartPlusMinusMultPolynom(ar, N, oneZeroArray, N2);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Smart mult polynom time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	superSmartPlusMinusMultPolynom(ar, N, oneZeroArray, N2);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("SuperSmart mult polynom time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	buggySuperSmartPlusMinusMultPolynom(ar, N, oneZeroArray, N2);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Buggy superSmart mult polynom with bug time==>%f ms\n", (finish - start) * 1000.0 / frequency);*/


	// Round
	/*float fAr[] = {16.4, 7.58, 10, -12.48, -25.7, -17};
	superSmartRound(fAr, 6);
	printf("%f %f %f %f %f %f\n", fAr[0], fAr[1], fAr[2], fAr[3], fAr[4], fAr[5]);*/

	/*float* floatAr = generateRandomFloatArray(N);
	float* floatAr2 = clone(floatAr, N);
	
	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	stupidRound(floatAr, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Stupid Round time==>%f ms\n", (finish - start) * 1000.0 / frequency);

	start = 0; finish = 0;
	QueryPerformanceCounter((LARGE_INTEGER *)&start);
	superSmartRound(floatAr2, N);
	QueryPerformanceCounter((LARGE_INTEGER *)&finish);
	printf("Super Smart Round time==>%f ms\n", (finish - start) * 1000.0 / frequency);*/


	// Predictor
	//int bTotal = 100000;
	////bool bAr[] = {1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0};
	////bool bAr[] = {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1};
	//bool *bAr = generateRandomBoolArray(bTotal);
	//int errors = predict(bAr, bTotal);
	//printf("total==>%i errors==>%i efficiency==>%f", bTotal, errors, (float)(bTotal - errors) / bTotal);



	getchar();

	return 0;
}